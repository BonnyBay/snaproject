import json
import re
from cmath import nan
from collections import defaultdict
from contextlib import nullcontext
from email import header
from logging import NullHandler

import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
import pandas as pd
import powerlaw
import seaborn as sns

### INFO ####
#    # G: is a MultiDiGraph => used to showthe numbers of edges between two nodes
#    # H: is a DiGraph => used for majority of features such as measures, plot, ...
### END INFO ####

labels = {}

list_with_max_k_cores = []

sommaTotIONumeratore = 0 #in degree totali
sommaTotIODenominatore = 0 #out degree totali 
rapportoTotale_InOutDegre = 0

def plot_tx_graph(H,pos,df_exchanges):

    #number of nodes and edges
    #print(H)

    #draw nodes
    colorVector_nodes = apply_color_exchange_node(H,df_exchanges)
    nx.draw_networkx_nodes(H,pos,node_size=50, node_color=colorVector_nodes )
    #draw labels
    labels = add_labels_to_node(H)
    nx.draw_networkx_labels(H,pos,labels,font_size=8)
    #set and draw the weighted edges
    draw_weight_edges(H, pos)

    #setting the color-legend
    color_list_legend = []
    for i in range(0,len(df_exchanges)):
        #insert the colors inside the legend
        color_list_legend.append(mpatches.Patch(color=df_exchanges['COLOR'][i], label=df_exchanges['EXCHANGE'][i]))
    plt.legend(handles=color_list_legend)
    plt.axis('off')
    plt.show()
    


# ------ GRAPH LABEL CREATION ------ #

def add_labels_to_node(G):
    hubs = []
    for node in G.nodes():
        degree = G.degree([node])
        if degree[node] > 3:
            hubs.append(node)
        if node in hubs:
            #set the node name as the key and the label as its value 
            labels[node] = node[:3]+"..."+node[(len(node)-3):]

    return labels

#add color for exchange nodes
def apply_color_exchange_node(G,df_EX): #df_EX is the csv where are located a specific addresses, with his names and colors 
    colorMap = []
    addr_ex = np.array(df_EX['ADDRESS']) #array of address located inside csv
    for node in G.nodes():
        if node in addr_ex:
            index = np.where(addr_ex == node) #get the index of the node in the csv (pandas dataframe)
            colorMap.append(df_EX.at[index[0][0],'COLOR']) #create a list of color, getting from csv
        else:
            colorMap.append('green')

    return colorMap

# ------ GRAPH EDGE WEIGHT ------ #

def draw_weight_edges(G, pos):
    all_weights = []
    for (node1,node2,data) in G.edges(data=True):
        all_weights.append(data['weight']) #we'll use this when determining edge thickness

    for weight in all_weights: #era: unique_weights non all_weights
        #Form a filtered list with just the weight you want to draw
        weighted_edges = [(node1,node2) for (node1,node2,edge_attr) in G.edges(data=True) if edge_attr['weight']==weight]
        width = weight
        #draw edges
        nx.draw_networkx_edges(G,pos,edgelist=weighted_edges,width=width)


# ------ GRAPH DRAW - POWER LAW - MEASURES ------ #

def draw_graph(senders, receivers, amount):

    G=nx.MultiDiGraph()
    #print(senders[2348])
    for i in range(0,len(receivers)):
        try:
            G.add_edge(senders[i],receivers[i],weight=amount[i])
        except KeyError:
            pass

    H = nx.DiGraph()
    for u,v,d in G.edges(data=True):
        w = d['weight']
        if H.has_edge(u,v):
            H[u][v]['weight'] += w
        else:
            H.add_edge(u,v,weight=w)
        
    pos = nx.random_layout(H)

    df_exchanges = pd.read_csv('wallet_exchanges.csv',header=0)
    df_exchanges.set_index('ADDRESS')

# ------ NEW BUYERS AND SELLERS MEASURE CALLED ------ #

    #small_worldness_sigma(H)

# ------ NEW BUYERS AND SELLERS MEASURE CALLED ------ #

    buy_sell_measure(H,df_exchanges)

# ------ RECIPROCITY MEASURES CALLED ------ #

    # ex_wallets = df_exchanges['ADDRESS']

    # reciprocity_whole_G = reciprocity(G,pd.DataFrame())

    # reciprocity_exchanges = reciprocity(G,df_exchanges)

# ------ GRAPH DRAW WITH WEIGHTED EDGE ON AMOUNT OF TRANSACION SUMMED ------ #

    #plot_tx_graph(H,pos,df_exchanges)

# ------ POWER LAW DISTRIBUTION CALLED ------ #

    #draw_powerLaw(G)

# ------ NODES' MEASURES - BET CEN, DEG CEN, PAGE RANK - CALLED ------ #

    #make_measure(H,df_exchanges)

# ------ KCores' MEASURE CALLED ------ #

    #get_kcore(H, len(receivers), labels) 

# ------ Clustering Coefficient measure called ------ #

    #get_clustering_coefficient(H, 0) #if the second parameters is 0, the method will execute on the entire graph



# ------ SMALL WORLDNESS COEFFICENT SIGMA CALC FUNCTION ------ #

def small_worldness_sigma(G):

    G = G.to_undirected()

    n = len(G.nodes())
    p = 0.05

    er_graph = nx.erdos_renyi_graph(n, p, directed=True)

    g = ""
    avg_path_len_g = 0


    for g in nx.connected_components(G):
        g = G.subgraph(g)
        #print(g)
        if len(g.nodes()) > 100:
            print(g)
            avg_path_len_g = nx.average_shortest_path_length(g)
            print (avg_path_len_g)

    avg_path_len_ER = nx.average_shortest_path_length(er_graph)

    cc_G = get_clustering_coefficient(g,0)
    cc_ER = get_clustering_coefficient(er_graph,0)

    print(cc_G," --- ",cc_ER," --- ", avg_path_len_g," --- ",avg_path_len_ER)

    sigma = (cc_G/cc_ER) / (avg_path_len_g/avg_path_len_ER)

    print(sigma)


# ------ NEW BUYERS AND SELLERS MEASURE ------ #

def buy_sell_measure(H,df_exchanges):

    #variables for plotting
    name_exchanges = []
    list_in_out_degree_compratori = []
    list_in_out_degree_venditori = []

    #var total in out degree for network
    totalBuyer = 0
    totalSeller = 0

    total_amountOut = 0
    total_amountIn = 0

    #ratio in/out degree for each platform
    list_of_in_out_degree = []
    list_of_colors = []


    for el in df_exchanges.iterrows():
        if H.has_node(el[1]['ADDRESS']):
            totalBuyer = totalBuyer + H.out_degree(el[1]['ADDRESS'])
            totalSeller = totalSeller + H.in_degree(el[1]['ADDRESS'])

            #calcolo la somma totale dei pesi per i nodi "in" e poi per i nodi "out"
            # for (u, v, data) in H.edges(data=True):
            #     #if node is a "IN" node
            #     total_amountIn += data["weight"] 
            # print(H.out_degree(el[1]['ADDRESS'], weight='weight'))
            # print(H.in_degree(el[1]['ADDRESS'], weight='weight'))
            total_amountOut += H.out_degree(el[1]['ADDRESS'], weight='weight')
            total_amountIn += H.in_degree(el[1]['ADDRESS'], weight='weight')

    print("out ",total_amountOut)
    print("in ",total_amountIn)

    for el in df_exchanges.iterrows():
        if H.has_node(el[1]['ADDRESS']):

            weighted_amount_out = H.out_degree(el[1]['ADDRESS'], weight='weight')/total_amountOut #every edge has a specific degree that will be moltiplicated for a weight that consider the amount (in and out) related to the total amount (in e out) for every nodes
            weighted_amount_in = H.in_degree(el[1]['ADDRESS'], weight='weight')/total_amountIn 

            weighted_ratio = H.out_degree(el[1]['ADDRESS'], weight='weight')/H.in_degree(el[1]['ADDRESS'], weight='weight')

            # out = buyer & in = seller
            weightBuyer = H.out_degree(el[1]['ADDRESS'], weight='weight')/total_amountOut
            weightSeller = H.in_degree(el[1]['ADDRESS'], weight='weight')/ total_amountIn

            name_exchanges.append(el[1]['EXCHANGE'])

            list_in_out_degree_compratori.append(weightBuyer)

            list_in_out_degree_venditori.append(weightSeller)

            print("", el[1]['EXCHANGE'],",",in_out_degree_node(H,el[1]['ADDRESS']), ",",weighted_ratio,",",weightBuyer*100,",",weightSeller*100)
            list_of_in_out_degree.append(in_out_degree_node(H,el[1]['ADDRESS']))
            #print("weightBuyer ", weightBuyer, " weightSeller ",weightSeller, "\n")
            if in_out_degree_node(H,el[1]['ADDRESS']) > 1: #sellers
                list_of_colors.append("red")
            else: #buyers
                list_of_colors.append("green")

    # ------------------ barplot of in/out nodes ------------------
    # df_plot = pd.DataFrame(np.c_[list_in_out_degree_compratori, list_in_out_degree_venditori], index=name_exchanges)
 
    # plt.pie(list_in_out_degree_compratori,labels=name_exchanges, autopct='%.1f%%')
    # plt.title("Move out edges | Buyer")
    # plt.show()

    # plt.pie(list_in_out_degree_venditori,labels=name_exchanges, autopct='%.1f%%')
    # plt.title("Move in edges | Sellers")
    # plt.show()

    # plt.bar(name_exchanges, list_of_in_out_degree, color=list_of_colors)
    # plt.title("Ratio of in/out (seller/buyer) degree")
    # plt.ylabel("In/Out ratio")
    # plt.show()

    # ex_wallets = df_exchanges['ADDRESS']

    # reciprocity_whole_G = reciprocity(G,pd.DataFrame())

    # reciprocity_exchanges = reciprocity(G,df_exchanges)

    # print(reciprocity_whole_G)
    # for el in reciprocity_exchanges:
    #     print(el)


# ------ Assortativity for node degree ------ #
    #print(check_assortativity_of_graph(H)) 


#add color for exchange nodes
def apply_color_exchange_node(G,df_EX): #df_EX is the csv where are located a specific addresses, with his names and colors 
    colorMap = []
    addr_ex = np.array(df_EX['ADDRESS']) #array of address located inside csv
    for node in G.nodes():
        #print(node)
        if node in addr_ex:
            index = np.where(addr_ex == node) #get the index of the node in the csv (pandas dataframe)
            colorMap.append(df_EX.at[index[0][0],'COLOR']) #create a list of color, getting from csv
        else:
            colorMap.append('green')

    return colorMap



# ------ ASSORTATIVITY MEASURES ------ #
def check_assortativity_of_graph(H):
    results = nx.degree_assortativity_coefficient(H)

    return results

# ------ NODES' MEASURES ------ #
def make_measure(G,df_ex):

    range_to_take_for_measures = 25
    top_dc = []
    top_bc = []
    top_pr = []

    deg_centrality = nx.degree_centrality(G)
    deg_centrality = sorted(deg_centrality.items(), key=lambda kv: kv[1], reverse=True)

    bet_centrality = nx.betweenness_centrality(G, normalized = True, endpoints = False)
    bet_centrality = sorted(bet_centrality.items(), key=lambda kv: kv[1], reverse=True)

    pr = nx.pagerank(G, alpha = 0.8)
    pr = sorted(pr.items(), key=lambda kv: kv[1], reverse=True) 

    addr_ex = np.array(df_ex['ADDRESS'])

    for i in range(0, range_to_take_for_measures):
        el_dc = deg_centrality[i][0]
        el_bc = bet_centrality[i][0]
        el_pr = pr[i][0]

        if el_dc in addr_ex:
            index = np.where(addr_ex == el_dc)
            el_dc = df_ex.at[index[0][0],'EXCHANGE']
        else:
            el_dc = el_dc[:2]+".."+el_dc[(len(el_dc)-2):]

        if el_bc in addr_ex:
            index = np.where(addr_ex == el_bc)
            el_bc = df_ex.at[index[0][0],'EXCHANGE']
        else:
            el_bc = el_bc[:2]+".."+el_bc[(len(el_bc)-2):]

        if el_pr in addr_ex:
            index = np.where(addr_ex == el_pr)
            el_pr = df_ex.at[index[0][0],'EXCHANGE']
        else:
            el_pr = el_pr[:2]+".."+el_pr[(len(el_pr)-2):]

        top_dc.append([el_dc,deg_centrality[i][1]])
        top_bc.append([el_bc,bet_centrality[i][1]])
        top_pr.append([el_pr,pr[i][1]])

    df_dc = pd.DataFrame(top_dc)
    df_bc = pd.DataFrame(top_bc)
    df_pr = pd.DataFrame(top_pr)

    dc_plot = sns.barplot(x = 0, y = 1, data = df_dc)
    dc_plot.set(xlabel='Wallet Address', ylabel='Degree Centrality', title='Degree Centrality for Wallets')
    dc_plot.set_xticklabels(df_dc[0], rotation=45)
    plt.show()

    bc_plot = sns.barplot(x = 0, y = 1, data = df_bc)
    bc_plot.set(xlabel='Wallet Address', ylabel='Betweenness Centrality', title='Betweenness Centrality for Wallets')
    bc_plot.set_xticklabels(df_bc[0], rotation=45)
    plt.show()

    pr_plot = sns.barplot(x = 0, y = 1, data = df_pr)
    pr_plot.set(xlabel='Wallet Address', ylabel='Page Rank', title='Page Rank for Wallets')
    pr_plot.set_xticklabels(df_pr[0], rotation=45)
    plt.show()



# ------ PRINT DEGREE CENTRALITY ------ #
    
    #print("deg centrality: ",json.dumps(deg_centrality, indent = 4) )


# ------ PRINT BETWEENNESS CENTRALITY ------ #

    #print("bet_centrality: ",json.dumps(bet_centrality, indent = 4) )

# ------ PRINT PAGE RANK ------ #
                               
    #print("Page Rank: ",json.dumps(pr, indent = 4) )


# ------ CLUSTERING COEFFICIENT FUNCTION ------ #

def get_clustering_coefficient(H, list_nodes):
    if list_nodes != 0:
        clustering_coeff_result = nx.algorithms.cluster.average_clustering(H, list_nodes)
        #print("Clustering coefficient for node with max K_cores of the graph: ",clustering_coeff_result)
        

    else:
        clustering_coeff_result = nx.algorithms.cluster.average_clustering(H) #on all the graph
        #print("Clustering coefficient for all nodes of the graph: ",clustering_coeff_result)

    return clustering_coeff_result
 
# ------ POWER LAW DISTRIBUTION HISTOGRAM FUNCTION ------ #

def draw_powerLaw(G):

    degree_sequence = sorted([d for n, d in G.degree()], reverse=True) # used for degree distribution and powerlaw test

    plt.xlabel('Degree')
    plt.ylabel('Fraction p_d of nodes with degree d')
    plt.yscale("log")
    plt.xscale("log")
    plt.hist(degree_sequence, bins="auto", color='green')
    plt.show()
    
    fit = powerlaw.Fit(degree_sequence, discrete=True, xmin=1)
    alpha = fit.alpha
    xmin = fit.xmin
    print("alpha: ", alpha, " xmin: ", xmin)
    axis = fit.plot_ccdf()
    fit.power_law.plot_ccdf(ax=axis, color='r', linestyle='--')
    plt.legend(['Network', 'Power law'])
    plt.xlabel("Degree")
    plt.ylabel("Fraction p_d of nodes with degree d or greater")

    plt.show()


# ------ K_CORES ------ #

def get_kcore(H,nodi,labels):
    nodi = range(1,nodi)
    k_degrees = range(1, 15)
    k_cores = [nx.k_core(
            H, k).order() for k in k_degrees]

    #print(k_cores_sequence)
    plt.plot(k_degrees, k_cores, marker="s")
    #print(k_cores)
    #print("\t kcors di 1 old version ",k_cores[1])
    plt.xlabel("K")
    plt.ylabel("Nodes")
    plt.show()

    #setting the graph for K_cores
    G = nx.from_pandas_edgelist(df, 'SENDER', 'RECEIVER')
    k_list = [] #list of k
    n_nodes_for_Each_k = [] #list of number of nodes for each K
    kcores = defaultdict(list) #container for k and addresses that have this key
    #create the list with k_kores using the k as index
    for n, k in nx.core_number(G).items():
        kcores[k].append(n)
        if (k not in k_list): #check if I have already inserted the K in the list
            k_list.append(k)

    #ordering the list (this has to be in this position, before the for that fill n_nodes_for_Each_k list)
    k_list.sort()

    for k in k_list:
        n_nodes_for_Each_k.append(len(kcores[k])) #add the number of nodes that have specidic K

    #lineplot
    plt.plot(k_list,n_nodes_for_Each_k , marker="s")
    plt.xlabel("K")
    plt.ylabel("Nodes")
    plt.show()

    # compute position of each node with shell layout
    pos = nx.layout.shell_layout(G, list(kcores.values()))
    colors = {1: 'green', 2: 'orange', 3: 'blue',4: 'green', 5: 'orange', 6: 'blue',7: 'green', 8: 'orange', 9: 'blue',10: 'green', 11: 'orange', 12: 'blue'}  

    # draw nodes, edges and labels
    for kcore, nodes in kcores.items():
        nx.draw_networkx_nodes(G, pos, nodelist=nodes, node_size=10, node_color=colors[kcore])

    nx.draw_networkx_edges(G, pos, width=0.2)
    nx.draw_networkx_labels(G, pos, labels, font_size=5)

    plt.show()

    max_k = max(k_list)
    list_cluster_coeff = []
    for i in k_list:
        list_with_max_k_cores = kcores[i] #list of nodes for specific k
        result = get_clustering_coefficient(H, list_with_max_k_cores)
        list_cluster_coeff.append(result)
    
    plt.plot(k_list,list_cluster_coeff , marker="s")
    plt.xlabel("K")
    plt.ylabel("Cluster Coefficient")
    plt.title("Cluster coefficient for every K_Cores")
    plt.show()



# ------ RECIPROCITY ------ #

def reciprocity(G, nodes):

    reciprocity = []

    if nodes.empty:
        reciprocity.append(['WHOLE GRAPH',nx.reciprocity(G)])
    else:
        addr_ex = np.array(nodes['ADDRESS'])
        for u in G.nodes():
            if u in addr_ex:
                i = np.where(addr_ex == u)
                reciprocity.append([nodes.at[i[0][0],'EXCHANGE'],G.in_degree(u),G.out_degree(u),nx.reciprocity(G, u)])
    
    return reciprocity


# ------ INNER AND OUTER DEGREE FOR A NODE ------ #
# if > 1, the node have more inner edge than outer,
# else viceversa 
# else if = 0: in_deg = 0 or in_deg==out_deg

def in_out_degree_node(G,node):

    in_d = G.in_degree(node)
    out_d = G.out_degree(node)
    
    if out_d == 0:
        return in_d
    else:
        return in_d/out_d



# ------ MAIN ------ #
if __name__ == '__main__':

    #name of csv
    #csv_name = "ATH/maj_100k/ATH-100_000"
    #csv_name = "BOTTOM/maj_100k/BOTTOM_100k+"
    csv_name = "ATH/maj_3000/ATH_3k+"
    #csv_name = "BOTTOM/maj_3000/BOTTOM_3k+"

    #read the csv
    df = pd.read_csv(csv_name+".csv")

    #setting variable 
    senders = df['SENDER']
    receivers = df['RECEIVER']
    amount = df['AMOUNT']

    for i in range(0,len(senders)):
        if senders[i] == receivers[i]:
            senders.pop(i)
            receivers.pop(i)
            amount.pop(i)

    # -- MIN-MAX SCALING FOR AMOUNTS -> [0.045,0.044] -- #
    # this choice has been taken because the thickness was too large 
    amount = np.array(amount)
    min_amount = np.min(amount)
    max_amount = np.max(amount)
    amount = ((amount - min_amount) / (max_amount - min_amount)) * (0.045 - 0.044) + 0.044     

    draw_graph(senders, receivers, amount)




























