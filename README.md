# SnaProject | MSc Computer Science: University of Bologna

Web Science | Social Network Analysis of Algorand Transactions
<p float="center">
  <p> <b>All Time Hight Algorand's blockchain transactions (December 2021)</b></p>
  <img src="ATH/maj_3000/green_3k+ATH.png" width="100%" />
  <br>
  <br>
  <p> <b>Crash period of January 2022 Algorand's blockchain transactions</b></p>
  <img src="BOTTOM/maj_3000/green_3k+BOTTOM.png" width="100%" /> 
</p>


## Project Description
In this work, we will present a study of the Algorand blockchain network considering two
periods of time: the All-Time High reached in December 2021 and the most important crash,
after its highest price, in January 2022.
Through metrics and measures, we have analyzed a small part of Algorand finding that the
centralized Exchanges are not the only nodes with which users interact, in addition to interacting
with each other.
Buying and selling operation was very frequent in the period that we have studied, anyways we
learn that people who transact amount greater than 3,000 Algos, the Algorand cryptocurrency,
tend to interact with platforms with different use cases such as staking on Yieldly instead
users with more than 100,000 Algos which interact mainly with exchanges for trading operations.
Another aim is to understand the factors that impact the price, such as trades/arbitrages of big
investors, and how users behave in different periods, e.g. interacting with each other or with
exchanges.

## Results
What emerged from this work consist on an analysis of how user and exchanges interact to each
other during interval of time characterized by high pressure.
Our work confirm the majority of our initial hypothesis such as the most popular exchanges are
the most used during the ATH for sell and during the crash period to buy.
An other initial hypothesis that we can confirm consist on the accumulating and staking from
the wealthy users (that means not big investors) using platform like Yieldly.
However, we have analyse small period of time and this could be improve on increasing it,
identifying pattern and other features.
We found that the impact on the Algo price is caused by people that transact at least 3,000 Algos
with exchanges. Indeed, although there are more transactions below this threshold, the amount
of Algos transacted greater than 3,000 overcome the sum of small transactions

## Authors and acknowledgment
Emanuele Fazzini & Michele Bonini

## License
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see https://www.gnu.org/licenses/.

## Project status
If we take the graph of degree for the ATH and lower Algo price (January 2022), we could
imagine that people selling during the All Time High and buying during the bottom phase.
So, this is the reason why in every three of the centrality measures (degree, Page Rank and
betweenness) there are Exchange in the first position such as Binance.
It could be interesting looking for a middle phase, where there aren’t so much buying or selling
pressure. Our hypothesis, in this case, concern the staking operation by users through platform
like Yieldly.
An other future development could consist on the study of the network taking more transactions,
increasing the filtering period (e.g. 30 days)
