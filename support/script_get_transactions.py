
from algosdk.v2client import algod
from algosdk.v2client import indexer
import requests 

import json
import pandas as pd


API_KEY = "nYYIqswHmq3C0xpWSNUYb8073dFpTUAu6tg1GXlw"
#headline asset id 137594422
ALGO_ASSET_ID = 0

after_time = "2022-01-21"
#before_time = "2022-01-20"
min_amount = 1000


url = 'https://mainnet-algorand.api.purestake.io/idx2/v2/assets/{0}/transactions?after-time={1}&currency-greater-than={2}&limit=1'.format(ALGO_ASSET_ID,after_time,min_amount)
headers = {
    "x-api-key":API_KEY
}
res1 = requests.get(url, headers=headers, timeout=None)
res = json.loads(res1.text)

print(json.dumps(res, indent=4, sort_keys=True))

#create csv
json_request = json.loads(json.dumps(res, indent=4, sort_keys=True))

#print(json_request['transactions'][0]['id'])


lista_amount = []
list_receiver = []
list_sender = []
for data in json_request['transactions']:
    amount = data['payment-transaction']['amount']
    receiver = data['payment-transaction']['receiver']
    sender = data['sender']
    lista_amount.append(amount)
    list_receiver.append(receiver)
    list_sender.append(sender)


name_dict = {
            'amount': lista_amount,
            'receiver': list_receiver,
            'sender': list_sender
          }

df = pd.DataFrame(name_dict)
df.to_csv('pony.csv')
